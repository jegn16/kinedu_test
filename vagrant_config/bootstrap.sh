sudo apt-get upgrade && sudo apt-get upgrade -y
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev mysql-server mysql-client libmysqlclient-dev nodejs build-essential sqlite3 libsqlite3-dev

cd /home/vagrant/
#ldconfig /usr/local/lib

#install nodejs
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt install -y nodejs

# install rbenv
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
source ~/.bashrc
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
eval "$(rbenv init -)"

#install ruby
rbenv install 2.6.0
rbenv global 2.6.0

#install bundler
gem install bundler -v 2.1.4
gem update --system

rbenv rehash

cd /vagrant

#Prepare dependencies of project
bundle install

#creates empty DB
rake db:create

mysql -u root --password=root kinedu_development  < /home/vagrant/database.sql
mysql -u root --password=root kinedu_test  < /home/vagrant/database.sql

sudo mysqladmin -proot variables | grep socket

echo "======== Deploy finished ========"