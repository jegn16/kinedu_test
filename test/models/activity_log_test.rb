require 'test_helper'

class ActivityLogTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  #

  test "Should not save empty Activity Log" do
    activity_log = ActivityLog.new
    assert_not activity_log.save, "Saved the empty activity log"
  end

  test "Should not save activity log with only baby_id" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id}
    assert_not activity_log.save, "Saved the activity log with only baby_id"
  end

  test "Should not save activity log with only assistant_id" do
    activity_log = ActivityLog.new
    activity_log.attributes = {assistant_id: assistants(:idona).id}
    assert_not activity_log.save, "Saved the activity log with only assistant_id"
  end

  test "Should not save activity log with only activity_id" do
    activity_log = ActivityLog.new
    activity_log.attributes = {activity_id: activities(:group).id}
    assert_not activity_log.save, "Saved the activity log with only activity_id"
  end

  test "Should not save activity log with only start_time" do
    activity_log = ActivityLog.new
    activity_log.attributes = {start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with only start_time"
  end

  test "Should not save activity log with only stop_time" do
    activity_log = ActivityLog.new
    activity_log.attributes = {stop_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with only stop_time"
  end

  test "Should not save activity log with only duration" do
    activity_log = ActivityLog.new
    activity_log.attributes = {duration: 28}
    assert_not activity_log.save, "Saved the activity log with only duration"
  end

  test "Should not save activity log with only comments" do
    activity_log = ActivityLog.new
    activity_log.attributes = {duration: "Lorem idsum..."}
    assert_not activity_log.save, "Saved the activity log with only comments"
  end

  test "Should create activity log" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id,
                               assistant_id: assistants(:idona).id,
                               activity_id: activities(:group).id,
                               start_time: "2020-02-01T16:00:00Z"}
    assert activity_log.save, "Could not save the activity log without stop_time, comments, duration, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with invalid string as start_time" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id,
                               assistant_id: assistants(:idona).id,
                               activity_id: activities(:group).id,
                               start_time: "invalid date"}
    assert_not activity_log.save, "Could save the activity log with invalid string as start_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with invalid date as start_time" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id,
                               assistant_id: assistants(:idona).id,
                               activity_id: activities(:group).id,
                               start_time: "0000-00-00T00:00:00Z"}
    assert_not activity_log.save, "Could save the activity log with invalid date as start_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with different format date as start_time" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id,
                               assistant_id: assistants(:idona).id,
                               activity_id: activities(:group).id,
                               start_time: "2020-02-01 16:00:00"}
    assert_not activity_log.save, "Could save the activity log with different format date as start_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not save activity log with nonexistant baby_id" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: 100, assistant_id: assistants(:idona).id, activity_id: activities(:group).id, start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with nonexistant baby_id"
  end

  test "Should not save activity log with baby_id as string" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: "a", assistant_id: assistants(:idona).id, activity_id: activities(:group).id, start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with baby_id as string"
  end

  test "Should not save activity log with nonexistant assistant_id" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id, assistant_id: 100, activity_id: activities(:group).id, start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with nonexistant assistant_id"
  end

  test "Should not save activity log with assistant_id as string" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id, assistant_id: "a", activity_id: activities(:group).id, start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with assistant_id as string"
  end

  test "Should not save activity log with nonexistant activity_id" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id, assistant_id: assistants(:idona).id, activity_id: 100, start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with nonexistant activity_id"
  end

  test "Should not save activity log with activity_id as string" do
    activity_log = ActivityLog.new
    activity_log.attributes = {baby_id: babies(:merritt).id, assistant_id: assistants(:idona).id, activity_id: "a", start_time: "2020-02-01T16:00:00Z"}
    assert_not activity_log.save, "Saved the activity log with activity_id as string"
  end

  test "Should not update activity logo with only comments" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {comments: "Lorem Idsum..."}
    assert_not activity_log.save, "Updated the activity log with only comments"
  end

  test "Should update activity logo with only stop_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T00:00:01Z"}
    assert activity_log.save, "Could not update the activity log with only stop_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should update activity logo with stop_time and comments" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T00:00:01Z", comments: "Lorem Idsum..."}
    assert activity_log.save, "Could not update the activity log with only stop_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with invalid string as stop_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "invalid date"}
    assert_not activity_log.save, "Could update the activity log with invalid string as stop_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with invalid date as stop_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-02-31T00:00:00Z"}
    assert_not activity_log.save, "Could save the activity log with invalid date as stop_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with different format date as stop_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-02-01 16:00:00"}
    assert_not activity_log.save, "Could save the activity log with different format date as stop_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should not create activity log with stop_time that happens before start_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2019-12-31T00:00:00Z"}
    assert_not activity_log.save, "Could update the activity log with stop_time that happens before start_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should create activity log with stop_time that happens simultaneously to start_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T00:00:00Z"}
    assert activity_log.save, "Could not update the activity log with stop_time that happens simultaneously to start_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should create activity log with stop_time that happens after start_time" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T12:00:00Z"}
    assert activity_log.save, "Could not update the activity log with stop_time that happens simultaneously to start_time, #{activity_log.errors.full_messages.join(",")}"
  end

  test "Should set duration on activity log after update to 30" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T00:30:00Z"}
    activity_log.save
    assert activity_log.duration == 30, "Activity log duration #{activity_log.duration} =/= 30"
  end

  test "Should set duration on activity log after update to 0" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T00:00:00Z"}
    activity_log.save
    assert activity_log.duration == 0, "Activity log duration #{activity_log.duration} =/= 0"
  end

  test "Should set duration on activity log after update to 1" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T00:01:45Z"}
    activity_log.save
    assert activity_log.duration == 1, "Activity log duration #{activity_log.duration} =/= 1"
  end

  test "Should set duration on activity log after update to 720" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-01T12:00:00Z"}
    activity_log.save
    assert activity_log.duration == 720, "Activity log duration #{activity_log.duration} =/= 720"
  end

  test "Should set duration on activity log after update to 4320" do
    activity_log = activity_logs(:test)
    activity_log.attributes = {stop_time: "2020-01-04T00:00:00Z"}
    activity_log.save
    assert activity_log.duration == 4320, "Activity log duration #{activity_log.duration} =/= 4320"
  end

end
