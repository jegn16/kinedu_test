require 'test_helper'

class BabyTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  #

  test "Should set baby age is 5 months old" do
    baby = babies(:test)
    baby.birthday = DateTime.now - 5.month
    assert baby.age == 5, "Baby age #{baby.age} =/= 5"
  end

  test "Should set baby age is 45 months old" do
    baby = babies(:test)
    baby.birthday = DateTime.now - 45.month
    assert baby.age == 45, "Baby age #{baby.age} =/= 45"
  end

  test "Should set baby age is 0 months old" do
    baby = babies(:test)
    baby.birthday = DateTime.now - 1.day
    assert baby.age == 0, "Baby age #{baby.age} =/= 45"
  end

  test "Should set baby age is 1 month old" do
    baby = babies(:test)
    baby.birthday = DateTime.now - 1.month
    assert baby.age == 1, "Baby age #{baby.age} =/= 1"
  end
end
