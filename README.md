# README

Project develop for a job application

Requirements:
- Ruby 2.6.0
- Nodejs v10+ or newer

Characteristics: 
- Rails 5.2.4.3
- 4 Web services
- 1 Administrative page

Extra features:
- Interactive web services documentation powered by swagger
- Unit test for models related to activity logs and babies
- Vagrantfile and vagrant_config/bootstrap.sh to deploy self-contain dev environment

Main page can be found at /activity_logs

Documentation of web services can be found at /api/docs


Run tests:
- Use <code>rails test</code> to run test. Currently there are 36 test and all should pass.

Notes:
- Database can be created with <code>rails db:create</code> but there are no migrations to create tables.
- Database schema is currently independent from project and need to be manually add through a .sql file.
- If deploy with vagrant, project will be ready to work with after vagrant ends deployment. (Database will be automatically populated)