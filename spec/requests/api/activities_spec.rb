require 'swagger_helper'

RSpec.describe 'activities', type: :request do

  path '/activities' do
    get 'Return a list of activities' do
      tags 'Activities'
      produces ' application/json'
      response '200', 'List of activities' do
        schema type: :array,
               items: {
                   type: :object,
                   properties: {
                       id: { type: :integer, example: 1, description: "Id that identifies the activity"},
                       name: {type: :string, example: "Actividad de grupo", description: "Name of activity"},
                       description: { type: :string, example: "A aliquet scelerisque per adipiscing proin id a condimentum scelerisque parturient a lobortis a eget condimentum venenatis parturient adipiscing velit dictumst."}
                   }
               }
        run_test!
      end

    end

  end

end
