require 'swagger_helper'

RSpec.describe 'babies', type: :request do

  path '/babies' do
    get 'Return a list off all babies' do
      tags 'Babies'
      produces 'application/json'
      response '200', 'List of babies' do
        schema type: 'array',
            items: {
                   type: 'object',
                   properties: {
                       id: { type: :integer, example: 1, description: "Id that identifies the baby"},
                       name: { type: :string, example: "Raul", decription: "Name of the baby"},
                       mother_name: { type: :string, example: "Sara", decription: "Name of the mother"},
                       father_name: { type: :string, example: "Jorge", decription: "Name of the father"},
                       phone: { type: :string, example: "1-446-909-2123", description: "Contact phone"},
                       address: { type: :string, example: "P.O. Box 351, 5555 Vivamus Road", description: "Contact address"},
                       age: { type: :number, example: "34", decription: "Age of baby (In months)"}
                   }
               }
        run_test!
      end

    end

  end

end
