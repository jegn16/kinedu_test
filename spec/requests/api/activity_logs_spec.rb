require 'swagger_helper'

RSpec.describe 'activity_logs', type: :request do

  path '/babies/{baby_id}/activity_logs' do
    get 'Returns a list of activities related to a specific baby' do
      tags 'Babies', 'Activity logs'
      produces 'application/json'
      parameter name: :baby_id, in: :path, type: :integer, description: "Id of a baby", example: 100
      response '200', 'List of activity logs relates to baby' do
        schema type: :array,
               items: {
                   type: :object,
                   properties: {
                       id: { type: :integer, example: 1, description: "Id identifying activity log"},
                       baby_id: {type: :integer, example: 100, description: "Id of baby"},
                       start_time: { type: :string, format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:25:37.000Z", description: "Time at witch activity started"},
                       stop_time: { type: :string, format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:37:37.000Z", description: "Time at witch activity finished", 'x-nullable': true},
                       assistant_name: { type: :string, example: "Kay Brewer", description: "Name of teacher"}
                   }
               }
        run_test!
      end
    end
  end

  path '/activity_logs' do
    post 'Create a new activity log' do
      tags 'Activity logs'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :activity_logs, in: :body, schema: {
          type: :object,
          properties: {
              activity_id: { type: :integer, description: "Id of activity been performed", example: 10},
              baby_id: { type: :integer, description: "Id of baby performing activity", example: 100},
              assistant_id: { type: :integer, description: "Id of assistant performing activity", example: 10},
              start_time: { type: :string, description: "Date and time in winch activity started", format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:25:37Z"}
          }
      }

      response '201', 'Activity created' do
        schema type: :object,
               properties: {
                   id: { type: :integer, example: 1, description: "Id identifying activity log"},
                   baby_id: {type: :integer, example: 100, description: "Id of baby"},
                   assistant_id: {type: :integer, example: 10, description: "Id of assistant"},
                   activity_id: {type: :integer, example: 8, description: "Id of activity"},
                   duration: {type: :integer, example: 45, description: "Duration of activity in minutes"},
                   comments: {type: :string, example: nil, description: "Comments provided by teacher on activity conclusion", 'x-nullable': true},
                   start_time: { type: :string, format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:25:37Z", description: "Time at witch activity started"},
                   stop_time: { type: :string, format: "YYYY-mm-ddTHH:ii:ssZ", example: nil, description: "Time at witch activity finished", 'x-nullable': true},
               }

        run_test!
      end

      response '422', 'Invalid Arguments' do
        schema type: :object,
               properties: {
                   errors: {
                       type: :array,
                       items: {
                           type: :string, example: "Start time is and invalid date. Allowed Format: YYYY-mm-ddTHH:ii:ssZ.", description: "Readable explanation or error"
                       }
                   }
               }
        run_test!
      end
    end
  end

  path '/activity_logs/{id}' do
    put 'Concludes activity log' do
      tags 'Activity logs'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :id, in: :path, description: "Id of activity log to conclude"
      parameter name: :activity_logs, in: :body, schema: {
          type: :object,
          properties: {
              stop_time: { type: :string, description: "Date and time in winch activity finished", format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:25:37Z"},
              comments: { type: :string, description: "Comments about the activity that was performed", example: "Lorem idsum..."}
          }
      }

      response '200', 'Activity created' do
        schema type: :object,
               properties: {
                   id: { type: :integer, example: 1, description: "Id identifying activity log"},
                   baby_id: {type: :integer, example: 100, description: "Id of baby"},
                   assistant_id: {type: :integer, example: 10, description: "Id of assistant"},
                   activity_id: {type: :integer, example: 8, description: "Id of activity"},
                   duration: {type: :integer, example: 45, description: "Duration of activity in minutes"},
                   comments: {type: :string, example: "Lorem idsum...", description: "Comments provided by teacher on activity conclusion"},
                   start_time: { type: :string, format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:25:37Z", description: "Time at witch activity started"},
                   stop_time: { type: :string, format: "YYYY-mm-ddTHH:ii:ssZ", example: "2017-04-03T14:37:37Z", description: "Time at witch activity finished"},
               }

        run_test!
      end

      response '422', 'Invalid Arguments' do
        schema type: :object,
               properties: {
                   errors: {
                       type: :array,
                       items: {
                           type: :string, example: "Stop time is and invalid date. Allowed Format: YYYY-mm-ddTHH:ii:ssZ.", description: "Readable explanation or error"
                       }
                   }
               }
        run_test!
      end

      response '404', 'Activity log not found' do
        run_test!
      end

    end

  end

end
