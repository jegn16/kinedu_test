---
openapi: 3.0.1
info:
  title: API V1
  version: v1
paths:
  "/activities":
    get:
      summary: Return a list of activities
      tags:
      - Activities
      responses:
        '200':
          description: List of activities
          content:
            " application/json":
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                      example: 1
                      description: Id that identifies the activity
                    name:
                      type: string
                      example: Actividad de grupo
                      description: Name of activity
                    description:
                      type: string
                      example: A aliquet scelerisque per adipiscing proin id a condimentum
                        scelerisque parturient a lobortis a eget condimentum venenatis
                        parturient adipiscing velit dictumst.
  "/babies/{baby_id}/activity_logs":
    get:
      summary: Returns a list of activities related to a specific baby
      tags:
      - Babies
      - Activity logs
      parameters:
      - name: baby_id
        in: path
        description: Id of a baby
        example: 100
        required: true
        schema:
          type: integer
      responses:
        '200':
          description: List of activity logs relates to baby
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                      example: 1
                      description: Id identifying activity log
                    baby_id:
                      type: integer
                      example: 100
                      description: Id of baby
                    start_time:
                      type: string
                      format: YYYY-mm-ddTHH:ii:ssZ
                      example: '2017-04-03T14:25:37.000Z'
                      description: Time at witch activity started
                    stop_time:
                      type: string
                      format: YYYY-mm-ddTHH:ii:ssZ
                      example: '2017-04-03T14:37:37.000Z'
                      description: Time at witch activity finished
                      x-nullable: true
                    assistant_name:
                      type: string
                      example: Kay Brewer
                      description: Name of teacher
  "/activity_logs":
    post:
      summary: Create a new activity log
      tags:
      - Activity logs
      parameters: []
      responses:
        '201':
          description: Activity created
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                    example: 1
                    description: Id identifying activity log
                  baby_id:
                    type: integer
                    example: 100
                    description: Id of baby
                  assistant_id:
                    type: integer
                    example: 10
                    description: Id of assistant
                  activity_id:
                    type: integer
                    example: 8
                    description: Id of activity
                  duration:
                    type: integer
                    example: 45
                    description: Duration of activity in minutes
                  comments:
                    type: string
                    example: 
                    description: Comments provided by teacher on activity conclusion
                    x-nullable: true
                  start_time:
                    type: string
                    format: YYYY-mm-ddTHH:ii:ssZ
                    example: '2017-04-03T14:25:37Z'
                    description: Time at witch activity started
                  stop_time:
                    type: string
                    format: YYYY-mm-ddTHH:ii:ssZ
                    example: 
                    description: Time at witch activity finished
                    x-nullable: true
        '422':
          description: Invalid Arguments
          content:
            application/json:
              schema:
                type: object
                properties:
                  errors:
                    type: array
                    items:
                      type: string
                      example: 'Start time is and invalid date. Allowed Format: YYYY-mm-ddTHH:ii:ssZ.'
                      description: Readable explanation or error
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                activity_id:
                  type: integer
                  description: Id of activity been performed
                  example: 10
                baby_id:
                  type: integer
                  description: Id of baby performing activity
                  example: 100
                assistant_id:
                  type: integer
                  description: Id of assistant performing activity
                  example: 10
                start_time:
                  type: string
                  description: Date and time in winch activity started
                  format: YYYY-mm-ddTHH:ii:ssZ
                  example: '2017-04-03T14:25:37Z'
  "/activity_logs/{id}":
    put:
      summary: Concludes activity log
      tags:
      - Activity logs
      parameters:
      - name: id
        in: path
        description: Id of activity log to conclude
        required: true
      responses:
        '200':
          description: Activity created
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                    example: 1
                    description: Id identifying activity log
                  baby_id:
                    type: integer
                    example: 100
                    description: Id of baby
                  assistant_id:
                    type: integer
                    example: 10
                    description: Id of assistant
                  activity_id:
                    type: integer
                    example: 8
                    description: Id of activity
                  duration:
                    type: integer
                    example: 45
                    description: Duration of activity in minutes
                  comments:
                    type: string
                    example: Lorem idsum...
                    description: Comments provided by teacher on activity conclusion
                  start_time:
                    type: string
                    format: YYYY-mm-ddTHH:ii:ssZ
                    example: '2017-04-03T14:25:37Z'
                    description: Time at witch activity started
                  stop_time:
                    type: string
                    format: YYYY-mm-ddTHH:ii:ssZ
                    example: '2017-04-03T14:37:37Z'
                    description: Time at witch activity finished
        '422':
          description: Invalid Arguments
          content:
            application/json:
              schema:
                type: object
                properties:
                  errors:
                    type: array
                    items:
                      type: string
                      example: 'Stop time is and invalid date. Allowed Format: YYYY-mm-ddTHH:ii:ssZ.'
                      description: Readable explanation or error
        '404':
          description: Activity log not found
          content: {}
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                stop_time:
                  type: string
                  description: Date and time in winch activity finished
                  format: YYYY-mm-ddTHH:ii:ssZ
                  example: '2017-04-03T14:25:37Z'
                comments:
                  type: string
                  description: Comments about the activity that was performed
                  example: Lorem idsum...
  "/babies":
    get:
      summary: Return a list off all babies
      tags:
      - Babies
      responses:
        '200':
          description: List of babies
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                      example: 1
                      description: Id that identifies the baby
                    name:
                      type: string
                      example: Raul
                      decription: Name of the baby
                    mother_name:
                      type: string
                      example: Sara
                      decription: Name of the mother
                    father_name:
                      type: string
                      example: Jorge
                      decription: Name of the father
                    phone:
                      type: string
                      example: 1-446-909-2123
                      description: Contact phone
                    address:
                      type: string
                      example: P.O. Box 351, 5555 Vivamus Road
                      description: Contact address
                    age:
                      type: number
                      example: '34'
                      decription: Age of baby (In months)
servers:
- url: http://{defaultHost}/api
  variables:
    defaultHost:
      default: localhost:3000
- url: https://{defaultHost}/api
  variables:
    defaultHost:
      default: localhost:3000
- url: https://kidenu-test.herokuapp.com/api
