class Baby < ApplicationRecord
  has_many :activity_logs

  def age
    diff = Date.today - self.birthday.to_date

    days = diff.to_i

    months = days / 30

    months
  end

end
