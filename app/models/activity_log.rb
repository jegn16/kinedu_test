class ActivityLog < ApplicationRecord
  belongs_to :assistant
  belongs_to :activity
  belongs_to :baby

  validates :baby_id, :assistant_id, :activity_id, :start_time, presence: true
  validates :baby_id, :assistant_id, :activity_id, numericality: { only_integer: true }
  validates :stop_time, presence: true, on: :update

  before_update :set_duration

  validates :start_time, iso8601_date_time_utc: true, on: :create
  validates :stop_time, iso8601_date_time_utc: true, on: :update

  validate :stop_time_later_than_start_time, on: :update

  def stop_time_later_than_start_time
    if self.stop_time.nil? || self.start_time > self.stop_time
      errors.add(:stop_time, "can not be earlier than: #{self.start_time.iso8601}")
    end
  end

  def set_duration
    diff_in_seconds = self.stop_time.to_time - self.start_time.to_time

    diff_in_minutes = diff_in_seconds / 60

    self.duration = diff_in_minutes

  end

end
