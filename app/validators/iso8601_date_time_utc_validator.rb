class Iso8601DateTimeUtcValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    begin
      raw_date_string = record.read_attribute_before_type_cast(attribute)
      raise ArgumentError if raw_date_string.nil?
      Rails.logger.info "DateTime to validate: #{raw_date_string}"
      casted_date = DateTime.strptime raw_date_string, "%Y-%m-%dT%H:%M:%S%z"
    rescue ArgumentError => error
      record.errors[attribute] << "is and invalid date. Allowed Format: YYYY-mm-ddTHH:ii:ssZ."
    end
  end
end