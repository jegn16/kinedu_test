class ActivityLog::SingleSerializer < ActiveModel::Serializer
  attributes :id, :baby_id, :assistant_id, :activity_id, :duration, :comments

  attribute :start_time do
    object.start_time.iso8601
  end

  attribute :stop_time do
    (object.stop_time.present?) ? object.stop_time.iso8601 : nil
  end
end

