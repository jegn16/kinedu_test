class ActivityLog::ListSerializer < ActiveModel::Serializer
  attributes :id, :baby_id

  attribute :start_time do
    object.start_time.iso8601
  end

  attribute :stop_time do
    (object.stop_time.present?) ? object.stop_time.iso8601 : nil
  end

  belongs_to :assistant, key: :assistant_name do
    object.assistant.name
  end
end
