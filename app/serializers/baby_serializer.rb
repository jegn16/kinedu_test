class BabySerializer < ActiveModel::Serializer
  attributes :id, :name, :mother_name, :father_name, :phone, :address, :age

end
