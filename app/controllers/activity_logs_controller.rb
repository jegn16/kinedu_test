class ActivityLogsController < ApplicationController


  # Todo list:
  # - Check months algorithm (Done)
  # - Remember applied filters on table (Done)
  # - Refactor as json on model (Done)
  # - Refactor time formatting as standard (Done)
  # - Verify is runnable on vagrant (Done)
  # - Update swagger docs (Done)
  def index
    @assistants = Assistant.all
    @babies = Baby.all
    @time_zone = "America/Monterrey"

    @activity_logs = ActivityLog.includes(:baby, :assistant, :activity).all

    @filters = filter_params

    if @filters[:baby_id].present?
      @activity_logs = @activity_logs.where(baby_id: @filters[:baby_id])
    end

    if @filters[:assistant_id].present?
      @activity_logs = @activity_logs.where(assistant_id: @filters[:assistant_id])
    end

    if @filters[:status].present?
      @activity_logs = @activity_logs.where(stop_time: nil) if @filters[:status] == "false"
      @activity_logs = @activity_logs.where.not(stop_time: nil) if @filters[:status] != "false"
    end

    @activity_logs = @activity_logs.order(start_time: :desc)

  end

  private
  def filter_params
    params.permit(:baby_id, :assistant_id, :status)
  end

end
