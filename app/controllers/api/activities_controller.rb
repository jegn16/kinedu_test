class Api::ActivitiesController < ApplicationController

  def index
    render json: Activity.all, each_serializer: ActivitySerializer
  end

end
