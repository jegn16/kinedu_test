class Api::BabiesController < ApplicationController

  def index
    render json: Baby.all, each_serializer: BabySerializer
  end

end
