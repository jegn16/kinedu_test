class Api::ActivityLogsController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index

    query = ActivityLog.all
    query = query.where(baby_id: params[:baby_id]) if params[:baby_id].present?

    render json: query.includes(:assistant), each_serializer: ActivityLog::ListSerializer

  end

  def create

    new_activity = ActivityLog.new
    new_activity.attributes = save_params

    if new_activity.valid?
      new_activity.save!

      render json: new_activity, serializer: ActivityLog::SingleSerializer, status: :created
    else
      render json: {errors: new_activity.errors.full_messages}, status: :unprocessable_entity
    end

  end

  def update

    activity_log = ActivityLog.find_by(id: params[:id])

    if activity_log.nil?
      return head :not_found
    end

    activity_log.attributes = update_params

    if activity_log.valid?
      activity_log.save!

      render json: activity_log, serializer: ActivityLog::SingleSerializer, status: :ok

    else
      render json: {errors: activity_log.errors.full_messages}, status: :unprocessable_entity
    end

  end

  private
  def save_params
    params.permit(:activity_id, :baby_id, :assistant_id, :start_time)
  end

  def update_params
    # Required to avoid scenario where request only contains comments and allows update
    # of activity log if this has already been previously closed
    default = { stop_time: nil, comments: nil}
    filtered_params = params.permit(:stop_time, :comments)

    default.merge filtered_params
  end

end
