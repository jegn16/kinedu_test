Rails.application.routes.draw do
  mount Rswag::Ui::Engine => 'api/docs'
  mount Rswag::Api::Engine => '/api-docs'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #

  root  to: redirect("/activity_logs")

  resources :activity_logs, only: [:index]

  namespace :api do
    resources :activities, only: [:index]
    resources :babies, only: [:index] do
      get '/activity_logs' => 'activity_logs#index'
    end

    resources :activity_logs, only: [:create, :update]
  end

  get '*patch',  to: "application#not_found_page"
end
